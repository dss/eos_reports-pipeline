# EOS reports parser

This is a backend application which reads EOS transfer reports from a configuration Kafka topic, turns them into nice ElasticSearch documents, and writes them to an output kafka topic.

## Instructions
 * The logstash configuration is in the `conf/` subdirectory
 * The pipelines are configured one per file in the `pipeline` subdirectory

## EOS Pipeline
 * The following environment variables can be set to alter logstash's behaviour:
   * `KAFKA_READ_SERVERS`: List of URLs to initially connect to the Kafka cluster in the format `host1:port1,host2:port2` (Defaults to `monit-kafka.cern.ch:9092`)
   * `KAFKA_WRITE_SERVERS`: List of URLs to initially connect to the Kafka cluster in the format `host1:port1,host2:port2` (Defaults to `monit-kafka.cern.ch:9092`)
   * `KAFKA_CLIENT_ID`: String to identify the client. Used to track the source of requests beyond host/ip (Defaults to `eosreports2es-client`)
   * `KAFKA_GROUP_ID`: Group to which the consumer belongs to. Used to distribute messages among instances with the same `group_id` (Defaults to `eosreports2es`)
   * `KAFKA_AUTOCOMMIT_INTERVAL`: Frequency in milliseconds that the consumer offsets are commited to Kafka (Defaults to `10000`)
   * `KAFKA_READ_TOPIC`: Topic to subscribe to (Defaults to `eos_logs`)
   * `KAFKA_WRITE_TOPIC`: Topic to write to (Defaults to `eos_logs_to_es`)
