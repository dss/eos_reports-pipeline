# See https://www.elastic.co/guide/en/logstash/current/docker.html
FROM docker.elastic.co/logstash/logstash-oss:6.6.1

# Install additional plugins
RUN /usr/share/logstash/bin/logstash-plugin install logstash-filter-prune
RUN /usr/share/logstash/bin/logstash-plugin install logstash-filter-fingerprint
#RUN /usr/share/logstash/bin/logstash-plugin remove logstash-output-kafka
#RUN /usr/share/logstash/bin/logstash-plugin install --version 7.1.1 logstash-output-kafka

RUN rm -f /usr/share/logstash/pipeline/logstash.conf
ADD pipeline/ /usr/share/logstash/pipeline/
